Weekly Priorities from 2021
This communicates to the 2 to 3 big rocks I am trying to accomplish this week usually in the style of OKRs with the heading being an objective and bullet points being key results to accomplish that. It will be updated weekly.

Legend
These designations are added to the previous week's priority list when adding the current week's priority.

Y - Completed
N - Not

# 2021-12-27 (Short Week)
1. **Y** - Move [competitive landscape research issue](https://gitlab.com/gitlab-com/Product/-/issues/3336) forward by merging the MR opened last week
1. **N** - Finalize Opportunity Canvas for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) using learnings from past research analysis 
1. **N** - Record reflective video from CEO Shadow weeks

# 2021-12-20
1. **Y** - Normal PM cadence work (PI review prep, 14.7 kickoff, 14.8 planning) and catchup
1. **N** - Move [competitive landscape research issue](https://gitlab.com/gitlab-com/Product/-/issues/3336) forward by merging the MR opened last week
1. **N** - Finalize Opportunity Canvas for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) using learnings from past research analysis 
1. **N** - Record reflective video from CEO Shadow weeks

# 2021-12-06 / 2021-12-13
1. CEO Shadow - My [coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/3430).

# 2021-11-29
1. **Y** - Finish filling in [Coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/3430) for CEO shadow
1. **N** - Finish [competitive landscape research issue](https://gitlab.com/gitlab-com/Product/-/issues/3336) forward by merging the MR opened last week
1. **N** - Draft Opportunity Canvas for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) using learnings from past research analysis

# 2021-11-22
1. **Y** - Move research forward for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) by mining existing research for Opportunity Canvas
2. **N** - Move [competitive landscape research issue](https://gitlab.com/gitlab-com/Product/-/issues/3336) forward by merging the MR opened last week
3. **Y** - Iterate on [Coverage issue](https://gitlab.com/gitlab-com/Product/-/issues/3430) for CEO shadow to schedule early items for next week

# 2021-11-15
1. **Y** - Contribute!
2. **Y** - Normal PM Cadence items (PI Review prep, 14.5 release posts, 14.6 kickoffs)
3. **N** - Move research forward for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) by mining existing research
4. **Y** - Move [competitive landscape research issue](https://gitlab.com/gitlab-com/Product/-/issues/3336) forward
    - [The MR](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/94275) to create the 2 new partials is started

# 2021-11-08
1. **Y** - Validate [direction issues](https://gitlab.com/gitlab-com/Product/-/issues/3353) for FY22::Q4
2. **N** - Move research forward for [Shared Runner Analysis](https://gitlab.com/gitlab-com/Product/-/issues/3311) by mining existing research
3. **Y** - Move [competitive landscape research issue](https://gitlab.com/gitlab-com/Product/-/issues/3336) forward
4. **Y** - Onboarding buddy todos for RK

# 2021-11-01
1. **Y** - Pipeline Execution Transition Issue todos
2. **Y** - Onboarding buddy todos for RK
3. **Y** - 14.6 prep for PE and Testing
    - Finalize planning issue(s)
    - Create refinement issue(s)
    - Write kickoff scripts / schedule recordings
    
# 2021-10-25
1. **Y** - Support Internship
2. **Y** - Pipeline Execution [Transition Issue](https://gitlab.com/gitlab-com/Product/-/issues/3256) todos
    - On track to finish up the week of 2021-11-01
3. **Y** - Wrap up User Journey research for testing feature setup
    - [Recomendation epic created](https://gitlab.com/groups/gitlab-org/-/epics/7012)
4. **Y** - Finalize Testing and PE Q4 OKRs

# 2021-10-18
1. **Y** - October Sales Enablement Session on Tuesday
2. **Y** - [Testing Team Transition](https://gitlab.com/gitlab-com/Product/-/issues/3160)
3. **Y** - Move Customer Journey research by finishing interviews and creating insights
4. **Y** - Support Internship
5. **Y** - Start [PE Team Transition](https://gitlab.com/gitlab-com/Product/-/issues/3256) issue

# 2021-10-11
1. **N** - [Transition issue](https://gitlab.com/gitlab-com/Product/-/issues/3160) Week 3 todos
    - I'll finish the walkthroughs of the direction pages this week.
2. **Y** - Support Internship
    - Did several interviews and talking through opportunity canvas contents now
3. **N** - Wrap up content for October Sales Enablement
    - Filling in some gaps but mostly wrapped up

# 2021-10-04
1. **Y** - [Transition issue](https://gitlab.com/gitlab-com/Product/-/issues/3160) Week 2 todos
2. **Y** - Support Internship
3. **Y** - Move [customer journey research](https://gitlab.com/gitlab-org/ux-research/-/issues/1552) forward
    - Create dovetail insights for last week's interviews
    - Upload any interviews from this week, create insights
    - Cancel any interviews scheduled after 10.22
4. **N** - Create remaining content and refine for October sales enablement session

# 2021-09-27
1. **Y** - [Transition issue](https://gitlab.com/gitlab-com/Product/-/issues/3160) Week 1 todos
    - Week 1 tasks completed
1. **Y** - Support Testing Internship
    - Interview recruting in progress putting us slightly behind schedule
1. **Y** - Move [customer journey research](https://gitlab.com/gitlab-org/ux-research/-/issues/1552) forward
    - **N** - Try to move interviews up
        - Partially done, some moved up and additional interviews scheduled for week of 09.27 and 10.04
    - **Y** - Schedule at least 1 more interview, preferably 3
    - **Y** - Finalize Discussion Guide
1. **N** - Create content for October Sales enablement Session
    - Partially assembled and on track for the 19th.

# 2021-09-20
1. **Y** - Move customer journey map research forward
    - 4 customer interviews scheduled in October
1. **Y** - Support Testing Internship
1. **N** - Draft abstracts for future commit sessions / partner sessions

# 2021-09-13
1. **N** - Move customer journey map research forward
1. **Y** - Support Testing Internship
1. **N** - Draft abstracts for future commit sessions / partner sessions
1. **Y** - Review 360 feedback, CDF review

# 2021-09-07 (short week/holiday)
1. **Y** - Move customer journey map research forward
    - Update [issue](https://gitlab.com/gitlab-org/ux-research/-/issues/1552) with research goal
    - Re-review existing usertesting.com research
1. **Y** - Support Testing Internship

# 2021-08-30
1. **Y** - [Sales Enablement Webcast Thursday](https://youtu.be/Zisa9Qfh8v8)
1. **N** - Move customer journey map research forward
1. **Y** - Wrap up Direction updates for Aug

# 2021-08-23 (short week)
1. **Y** - Complete 360 Reviews
1. **Y** - Kickoff research to create customer journey map from GitLab Testing Awareness -> View Test Summary 
1. **Y** - Update Sales Enablement Talk for upcoming webcast

# 2021-08-16
1. **Y** - [14.3 Kickoff prep](https://gitlab.com/gitlab-com/Product/-/issues/2947)
1. **N** - Review [past research](https://dovetailapp.com/projects/1dcaa44f-5200-432a-8df0-809d06de43c6/data/b/0c8c11e7-9941-4d04-9ed8-fc553af57849) / start research request for artifact usage
1. **Y** - Catchup from a few days off / loose ends that have accumulated

# 2021-08-09 (Short week/PTO)
1. **N** - Monthly Kickoff prep
1. **Y** - Create Draft Deck for [Sales Enablement Webinar](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/139) later in the month
1. **N** - [Review apps handoff](https://gitlab.com/gitlab-com/Product/-/issues/2933) 

# 2021-08-02
1. **Y** - Support [Internship for Learning for Scott S](https://gitlab.com/gitlab-com/people-group/Training/-/issues/1218)
1. **N** - Create Draft Deck for [Sales Enablement Webinar](https://gitlab.com/gitlab-com/sales-team/sales-enablement-videocast-series/-/issues/139) later in the month
1. **Y** - Support [partnership discussions](https://gitlab.com/gitlab-com/Product/-/issues/2897) as needed

# 2021-07-26
1. **Y** - Support [Solution Validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1484) analysis to move MVC forward for Project Quality Summary
1. **Y** - Prep work for next partnership discussion
1. **Y** - Wrap-up Personal Q2 Quarterly Goals issue and iterate on Q3 Goals
1. **Y**- Outreach to users previously interacted with for continuous interviewing purposes

# 2021-07-19
1. **Y** - [Project Leto](https://docs.google.com/document/d/1FtDlaU7AmstnAXk7AMYbJfsKmQMiYSeuL9lW1m2RN2w/edit#heading=h.ug2tleh5aifx) related work/meetings.
1. **Y** - [Solution Validation interviews](https://gitlab.com/gitlab-org/ux-research/-/issues/1484)
1. **Y** - Prep for Performance Indicator review next week
    1. The review was canceled but updates were made.


# 2021-07-12
1. **Y** - [Marketing event](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5124) prep/completion.
1. **N** - [Project Leto](https://docs.google.com/document/d/1FtDlaU7AmstnAXk7AMYbJfsKmQMiYSeuL9lW1m2RN2w/edit#heading=h.ug2tleh5aifx) related work/meetings.
1. **Y** - PM onboarding buddy/Designer onboarding chats and help as needed
1. **Y** - 14.1 wrapup and 14.2 kickoff related tasks

# 2021-07-05
1. **N** - Deliver [Metrics report demo project](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/66) 
    - Created and in reveiw from the team.
1. **Y** - Source more customers for [Project Quality Dash Solution Validation](https://gitlab.com/gitlab-org/ux-research/-/issues/1485)
    - Was able to book one additional customer.

# 2021-06-28
1. **Y** - Record a new [12 month direction update](https://www.youtube.com/watch?v=QiaU0TSC82w&t=2s)
1. **Y** - [Analyst Call](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/4385) and any follow-ups from it for direction pages
1. **Y** - Prep for [Marketing event](https://gitlab.com/gitlab-com/marketing/strategic-marketing/product-marketing/-/issues/5124) in 2 weeks

# 2021-06-21
1. **N** - Record a new 12 month direction update
1. **Y** - Move research for [Project Quality Dash MVC](https://gitlab.com/groups/gitlab-org/-/epics/5430) forward
    - Screener created and tarted outreach to customers who have expressed interest.

# 2021-06-14
1. **N** - Record a new 12 month direction update
2. **Y** - Normal Cadence items ([14.0 release](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/64), [14.1 kickoff](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/58), [June PI Review Prep](https://gitlab.com/gitlab-com/Product/-/issues/2689))
3. **Y** - Button up the [epic](https://gitlab.com/groups/gitlab-org/-/epics/5380) for artifact_size work

# 2021-06-07
1. **Y** - Candidate interviews and scorecards
1. **N** - Record a new 12 month direction update
1. **Y** - Catch up on priorities and issues for Build Artifacts category

# 2021-05-24
1. **N** - Get to merge/close for open category MRs ([Merge Load and Browser Testing](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/82758), add [Build Artifacts](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/82762), add [Testing in Production](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/80183))
    * Performance Testing and Build Artifacts were merged
1. **Y** - Code Testing and Coverage survey follow-up interviews
1. **Y** - Prep for week out

# 2021-05-17
1. **Y** - Prep for Performance Indicator review
1. **Y** - Schedule customer interviews for test history research
1. **Y** - Finish transition work for Code Quality to Static Analysis team

# 2021-05-10
1. **Y** - Get 13.12 Release Post items wrapped up and merged
1. **Y** - [Business Case](https://gitlab.com/gitlab-com/Product/-/issues/2516) for shift left testing staffing
1. **Y** - [Code Quality handoff](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1614)

# 2021-05-03
1. **Y** - [Shot #2!!](https://twitter.com/jheimbuck/status/1389654412495253506)
1. **Y** - [Create insights](https://dovetailapp.com/projects/b4c5425b-1ec0-49bd-8ce6-d825c875f926/insights) from internal customer calls about Code Quality Features
1. **N** - Create Testing in Production research issue and dig into existing research more
1. **Y** - Help wrapup Testing [Q1 OKRs](https://gitlab.com/groups/gitlab-com/-/epics/1375), draft [Q2 OKRs](https://gitlab.com/groups/gitlab-com/-/epics/1464).

# 2021-04-26
1. **Y** - Prep for and conduct 2 internal customer interviews for Code Quality category maturity
1. **Y** - Prep/follow-ups from customer calls (internal, external), PI review, other group/section calls and open category issues

# 2021-04-19
1. **N** - Recruit / Schedule internal customers for Code Quality Category Maturity Interviews
    - Was able to recruit and schedule 2 but we will need to do additional recruiting or externally recruit candidates for the rest.
1. **Y** - Direction updates post 13.11
1. **Y** - Draft "Testing in Production" category page for feedback

# 2021-04-12
1. **Y** - Get vaccinated!!
1. **N** - Recruit / Schedule internal customers for Code Quality Category Maturity Interviews
1. **Y** - Wrap up work on milestone 13.11 and prep for 13.12
1. **Y** - Record [pitch deck](https://www.youtube.com/watch?v=0JOWxY1jzLI) / [code quality](https://www.youtube.com/watch?v=4pJcrvZfGQU) walk through for Sales / SA use

# 2021-04-05
1. **Y** - Support any last minute requests / information gathering for Analyst review this week.
1. **Y** - Analysis of Code Quality JTBD Survey data.
1. **N** - Analysis of Code Testing and Coverage Survey data.
1. **N** - Open an MR to better document the process of a category name change.

# 2021-03-29
1. **Y** - Review upcoming work to prioritize impactful no/low design issues.
1. **N** - Analysis of survey data from Code Testing and Coverage survey.
1. **Y** - Shephard MRs to update [13.9](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78286) and [13.10](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/78328) Release Posts to update 14.0 date.
1. **N** - Open an MR to better document the process of a category name change.

# 2021-03-22
1. **Y** - Update [usage ping definitions for testing.](https://gitlab.com/gitlab-org/gitlab/-/issues/321824)
1. **Y** - Help as needed with IDC demo/survey.
1. **Y** - Finalize any more direction updates / PI meeting updates.

# 2021-03-15
1. **N** - Update [usage ping definitions for testing](https://gitlab.com/gitlab-org/gitlab/-/issues/321824).
    1. I didn't realize this was scheduled in 13.11 so deferred to next week.
1. **Y** - Merge [March PI update](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/76678)
1. **Y** - Release Related items ([13.10 release posts](https://about.gitlab.com/releases/2021/03/22/gitlab-13-10-released/#merge-request-test-summary-usability-improvements), [13.11 kickoff recording](https://www.youtube.com/watch?v=h87lBrRNu3k&list=PL05JrBw4t0KradvCrf8fL2pggEn5D3w2k&index=1&t=1s), [14.0 refinement kickoff](https://www.youtube.com/watch?v=HvjfO8gR9r4&list=PL05JrBw4t0Kq53VUOvTk3VdXN79PA0SXT&index=1) recording, start [direction items](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/77855))

# 2021-03-08
1. **N** - Release related items (Speed run for group code coverage, draft 13.10 release posts, 13.11 and 14.0 refinement)
1. **Y** - Anything required for current analyst questionnaire
1. **Y** - Prep and participation in Group and Stage Think Big sessions 

# 2021-03-01
1. **Y** - Complete PM Pitch Deck to [review with SAs this week](https://www.youtube.com/watch?v=-47KEaFFz8Y&t=5s)
1. **N** - Release related items (Speed run for group code coverage, draft 13.10 release posts, 13.11 and 14.0 refinement)
1. **Y** - Prep for and facilitate my group's Defining Business Outcomes session

# 2021-02-08
1. **N** - Move Code Testing and Coverage research forward by writing and fielding a survey
1. **N** - Move Code Quality JTBD research forward by reviwing the existing survey and resending to a new panel
1. **N** - Kickoff some informal research into Visual Review tools as a buying decision driver

# 2021-02-01
1. **N** - Move Code Testing and Coverage research forward by writing and fielding a survey
1. **N** - Move Code Quality JTBD research forward by reviwing the existing survey and resending to a new panel
1. **Y** - Prep for Ops PI review

# 2021-01-25
1. **Y** - Record speed run(s) for recent [Code Testing and Coverage Features](https://www.youtube.com/watch?v=QYgCfO_g7_U&feature=youtu.be)
1. **N** - Kickoff Research about why [users are not uploading unit test reports](https://gitlab.com/gitlab-org/ux-research/-/issues/1251) or coverage reports.
1. **N** - Restart [JTBD research](https://gitlab.com/gitlab-org/ux-research/-/issues/1083) for Code Quality

# 2021-01-18
1. **Y** - Record a 12 month vision update - [Oct 2020 version](https://www.youtube.com/watch?v=Mf6r_Infg8w&list=PL05JrBw4t0Kq53VUOvTk3VdXN79PA0SXT&index=12&t=2s)
    - [Uploaded](https://youtu.be/Yw_ieZO_6Y4)
1. **N** - Kickoff Research about why [users are not uploading unit test reports](https://gitlab.com/gitlab-org/ux-research/-/issues/1251) or coverage reports.
1. **N** - Restart [JTBD research](https://gitlab.com/gitlab-org/ux-research/-/issues/1083) for Code Quality

# 2021-01-11
1. **Y** - Finalize Category Maturity research by getting last interview completed, scorecard submitted and [maturity updated](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/71951).
1. **Y** - Finalize Scheduling/Messaging for artifact expiration date change on GitLab.com.
1. **Y** - Move our PI tracking efforts further forward by graphing what we can / helping the team wrap up the 13.8 issues.

# 2021-01-04
1. **Y** - Prep for [Monthly PI meeting](https://gitlab.com/gitlab-com/Product/-/issues/1929) and facilitate [ThinkBig with Package](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/31) to talk about coverage stats in a monorepo.
1. **Y** - Move [Category Maturity research](https://gitlab.com/gitlab-org/ux-research/-/issues/1034) forward by getting last interview with developer persona scheduled (internal or external).
1. **N** - Finalize Scheduling/Messaging for [artifact expiration date](https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9883#status-2020-12-28) change on GitLab.com
