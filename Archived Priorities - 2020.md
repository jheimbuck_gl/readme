## Weekly Priorities from 2020

This communicates to the 2 to 3 big rocks I am trying to accomplish this week usually in the style of OKRs with the heading being an objective and bullet points being key results to accomplish that. It will be updated weekly. 

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not 

# 2020-12-28
1. **N** - Move [Category Maturity research](https://gitlab.com/gitlab-org/ux-research/-/issues/1034) forward by getting last interview with developer persona scheduled (internal or external).
1. **Y** - Finalize MR for review to add a competency for [Creating an Opportunity Canvas](https://about.gitlab.com/handbook/product/product-manager-role/#validation-track-competencies)
    - [Draft MR for the competency](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/70806).

# 2020-12-21
1. **Y** - Move [Category Maturity research](https://gitlab.com/gitlab-org/ux-research/-/issues/1034) forward for Code Testing and Coverage by creating insights from [existing interviews](https://dovetailapp.com/projects/58ce8e49-3978-4459-ade2-1085cbc86186/data/b/261cb302-adb8-4bde-8c0e-6f5969cd9aee).
1. **Y** - Create a draft MR to add a competency for [Creating an Opportunity Canvas](https://about.gitlab.com/handbook/product/product-manager-role/#validation-track-competencies)

# 2020-12-14
1. **Y** - Move [Category Maturity research](https://gitlab.com/gitlab-org/ux-research/-/issues/1034) forward for Code Testing and Coverage by completing two more interviews with internal stakeholders and scheduling 1 or more additional external interview.
1. **Y** - Move [Code Quality JTBD](https://gitlab.com/gitlab-org/ux-research/-/issues/1083) project forward by making a plan to get more survey responses.
1. **Y** - Work with UX to identify design / research needs for next 3 milestones.

# 2020-12-07
1. **Y** - Catchup from [time off](https://gitlab.com/gitlab-com/Product/-/issues/1735)
1. **N** - Move [Category Maturity research](https://gitlab.com/gitlab-org/ux-research/-/issues/1034) forward for Code Testing and Coverage by completing two interviews and scheduling the remaining three.
1. **Y** - Prep release post items, PI updates, 13.8 kickoff item

# 2020-11-09
1. Release post manager duties

## 2020-11-02
1. **Y** - Prep for next week's Performance Indicator review
1. **N** - Work with UX recruiting to find additional candidates for Code Testing and Coverage interviews.
    1. The Code Quality JTBD survey was launched this week but held off on more recruiting externally for Code Testing

## 2020-10-26
1. **Y** - Wrapup as possible with 13.6 release posts / 13.7 planning to prep to be release post manager
1. **N** - Work with UX recruiting to find additional candidates for Code Testing and Coverage interviews.
1. **N** - Get Code Quality JTBD survey sent
    1. Survey prepped and ready pending recruiting availability

## 2020-10-12
1. **N** - Recruit and schedule JTBD interviews with internal customers for Code Testing and Coverage interviews. Any help is appreciated!
1. **N** - Move JTBD research for Code Quality forward by creatin the survey for release later in Oct.
1. **Y** - Wrapup any 13.6 refinement questions, write the release post items

## 2020-10-05
1. **Y** - Record Q4 12 month vision for Testing categories using 1 year vision designs - https://youtu.be/Mf6r_Infg8w
1. **Y** - Release related duties (13.5 RPI, 13.6 refinement, [13.7 planning issue](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/20))
1. **N** - Recruit and schedule JTBD interviews with internal customers for Code Testing and Coverage interviews. Any help is appreciated!
1. **N** - Move JTBD research for Code Quality forward by creatin the survey for release later in Oct.

## 2020-09-28
1. **Y** - Refine 13.6 issues and start team refinement. 
1. **N** - Record Q4 12 month vision for Testing categories using 1 year vision designs
1. **Y** - Do additional recruiting for the [Developer](https://gitlab.fra1.qualtrics.com/jfe/form/SV_4TP9E3qsAcZZlhr) and [Team Lead](https://gitlab.fra1.qualtrics.com/jfe/form/SV_6olEwbUGXFZ7zSt) surveys for Code Testing and Coverage JTBD

## 2020-09-21
1. **Y** - Direction page updates including [1yr vision designs](https://gitlab.com/gitlab-org/gitlab-design/-/issues/1217)
1. **Y** - ThinkBig (Tue) and Internal stakeholder (Thur) calls and follow-ups
1. **N** - Refine 13.6 issues and start team refinement. 

## 2020-09-14
1. **Y** - Finalize surveys / recruiting for external survey takers of Code Testing and Coverage JTBD research
1. **Y** - Kickoff Code Quality JTBD work with Testing Team
1. **N** - Record and publish walk through of Visual Reviews for Docs Page Update

## 2020-09-07
1. **N** - Publish survey to recruited participants for Code Testing and Coverage JTBD research
1. **Y** - Milestone related todos (Release Post Items, Kickoff Video)
1. **N** - Record and publish walk through of Visual Reviews for Docs Page Update

## 2020-08-31
1. **Y** - Publish survey to team for voting on Code Testing and Coverage JTBD
1. **N** - Finish Opportunity Canvas updates for Code Coverage validation
1. **N** - Record and publish walk through of Visual Reviews for Docs Page update

## 2020-08-24
1. **Y** - Move the Category Maturity Scorecard process forward for Code Testing and Coverage
1. **N** - Wrapup problem validation for Code Coverage Reports
1. **Y** - Move Forward with plans for a non-engineering project for improving PGMAU

## 2020-08-17
1. **N** - Synthesize learnings from Code Coverage Problem Validation interviews, create Opportunity Canvas
1. **N** - Firm up the plan and what we want to learn from the Test History MVC for inclusion in the 13.5 milestone
1. **N** - Identify and kickoff a non engineering effort to raise awareness of a testing feature that can increase PGMAU.

## 2020-08-10
1. **Y** - Hold Code Coverage Problem Validation interviews / recruit for 2 more
1. **N** - Kickoff CMS for Code Testing and Coverage in to get a handle on if we will hit the scheduled maturity move / find gaps.
1. **Y** - Enjoy Friends and Family Day!!

## 2020-08-03
1. **Y** - Create [insights in Dovetail](https://dovetailapp.com/projects/03c66d25-6321-4bc2-be55-44f2507006c1/data/b/c0b64f1c-7e31-4859-bbe7-5e89bf9dd73d) for Test History Solution Validation Interviews / update the epic accordingly.
1. **N** - Schedule 3+ interviews for Code Coverage Report Problem Validation.
    1. Scheduled 2 interviews for the week of 2020-08.10 but need to find a few more participants to have confidence we understand the problem.
1. **Y** - Any follow-ups from Think Big, PI review or CAB meetings this week.
    1. [Think Big issue with follow-ups](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/13)


## 2020-07-27
1. **Y** - Schedule 3 interviews for Test History Solution Validation.
1. **Y** - Create research items for Code Coverage Report Opportunity.
1. **Y** - Prep for and act on any follow-ups after GC, Think Big and Internal Customer meetings this week.

## 2020-07-20
1. **Y** - Wrap up 13.2 duties with direction page updates and next steps from feedback issues.
1. **N** - Try out the new [Iterations Feature](https://docs.gitlab.com/ee/user/group/iterations/)
    - The team is still discussing using this feature.
1. **N** - Get Test History Screener published
    - Screener is in Qualtrics and will be published ASAP.

## 2020-07-13
1. **Y** Prep / Complete Inquiry on Direction of Code Testing and Coverage
1. **Y** Complete any follow-ups (direction updates, issues) from the second part of Test History Think Big
1. **Y** Write Discussion guide for Test History Validation Interviews

## 2020-07-06
1. **Y** - Move Test History research forward with Think Big and Scheduling interviews
1. **Y** - Get remaining 13.2 Release Post items into review
1. **N** - Finalize 13.3 issues for kickoff video

## 2020-06-22
1. **Y** - Prep for North Star Metric review
1. **Y** - Prep for next Testing Think Big - [Historic Test Data](https://docs.google.com/document/d/1HB-ig3wgMdAipxsU9JiGxUXSc91XS04zzUyemjw5Adk/edit#heading=h.u2go2ithu2gk)
1. **Y** - Finalize Drafts for all 13.3 Items

## 2020-06-15
1. **Y** - Complete remaining 360 Feedback Reviews
1. **N** - Start validation for [Historic Test Data for Projects](https://gitlab.com/groups/gitlab-org/-/epics/3129)
1. **Y** - Start [13.3 release planning item](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/6)

## 2020-06-08
1. **N** - Complete remaining 360 Feedback Reviews
   1. **Y** Complete my own
   1. **N** Complete 5 of the 10 that have been requested
1. **N** - Get all 13.1 release post items into review
1. **N** - Finish work on 13.2 release planning item

## 2020-06-01
1. **Y** - More JTBD work
   1. Populate one customer JTBD per category
   1. Update Accessibility based on scorecard results
1. **N** - Complete some 360 Feedback Reviews
   1. Complete my own
   1. Complete 5 of the 10 that have been requested

## 2020-05-25
1. **Y** - Direction updates post 13.0 / other learnings
    * https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/51067
1. **N** - More JTBD work
   1. Populate one customer JTBD per category
   1. Update Accessibility based on scorecard results

## 2020-05-18
1. **N** - Direction updates post 13.0 / other learnings
1. **Y** - Create [JTBD page](https://about.gitlab.com/handbook/engineering/development/ci-cd/verify/testing/JTBD/) for Testing Group Handbook page

## 2020-05-11
1. **Y** - Move North Star epic out of planning into execution
   * Create issues in the Telemetry epic before Wednesday NSM review
1. **Y** - Finish Opportunity canvas for [Test Execution History](https://docs.google.com/document/d/1RStD5tF-vJVVR2zR-p_YJq0K1C6hc1696I1TgWWh1gM/edit#)
1. **Y** - Get epics on roadmap into a realistic state, not everything will be ready by 13.2
   * [Testing epic roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=group%3A%3Atesting&layout=MONTHS)

## 2020-04-27
1. **N** - **Move North Star epic out of planning into execution**
   * Create issues in the Telemetry epic
   * Follow-ups from Monday Product Meeting 
1. **Y** - Kickoff problem validation for [Historic Unit Test Insights](https://gitlab.com/groups/gitlab-org/-/epics/3129)
   * Draft [Opportunity Canvas](https://docs.google.com/document/d/1RStD5tF-vJVVR2zR-p_YJq0K1C6hc1696I1TgWWh1gM/edit?usp=sharing)
1. **N** - Publish blog for [a11y walk through](https://www.youtube.com/watch?v=LsW5D5HhuyE)
   * Waiting on last review and ready to publish.

## 2020-04-20
1. **N** - Move North Star epic out of planning into execution
   * Create issues in the Telemetry epic
   * Follow-ups from Monday Product Meeting
1. **Y** - Virtual Contribute

## 2020-04-13
1. **Y** - Wrap-up 13.0 Planning
   * Record Kickoff Recording
   * Wrap-up work on [13.0 Planning Issue](https://gitlab.com/gitlab-org/ci-cd/testing-group/-/issues/2)
1. **Y** - Move into Design/Solution validation for Group level Coverage and Test Data
   * Complete User Story mapping for MVC of Group Coverage Data
   * Kickoff Design with UX
1. **N** - Move North Star epic out of planning into execution
   * Create issues in the Telemetry epic
   * Follow-ups from Monday Product Meeting
