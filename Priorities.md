## Weekly Priorities

This communicates to the 2 to 3 big rocks I am trying to accomplish this week usually in the style of OKRs with the heading being an objective and bullet points being key results to accomplish that. It will be updated weekly. 

### Legend
These designations are added to the previous week's priority list when adding the current week's priority.
* **Y** - Completed
* **N** - Not 

## 2023-11-20 (short week for US Holiday)
1. Continue moving SKU work forward by getting more approvals and scheduling Eng work
1. Put blog into final review/publish
1. Move AI Enabled OKR forward by scheduling first evaluation issues

## 2023-11-13
1. **Y** - Scope work for Product Analytics SKU with Engineering, start getting approvals
    - Started gathering approvals and have signoff on the proposed flow.
1. **N** - Put blog into final review/publish
1. **Y** - Create a new demo for Field/Customer outreach with custom events and Visualization Designer
    - [Demo recording](https://www.youtube.com/watch?v=V4_y6Ovo780)
1. **N** - Move AI Enabled OKR forward by scheduling first evaluation issues
1. *Y** - Cadence PM work, kickoff calls, themes and objectives for next quarter's milestones

## 2023-11-06
1. **Y** - Scope work for Product Analytics SKU with Finance
1. **N** - Create a new demo for Field/Customer outreach with custom events
1. **Y** - Define evaluation criteria of Experiments for a [Q4 OKR](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4558)
1. **Y** - Next steps to get to 20 customer interactions this quarter

## 2023-10-30
1. **N** - Get blog draft into review by blog team
    - On hold for now.
1. **N** - Create a new demo for Field/Customer outreach with custom events
1. **Y** - Synthesize TBTS sessions for the team, create issues
1. **Y** - [Epic](https://about.gitlab.com/handbook/product/product-processes/#epics)/[issue](https://handbook.gitlab.com/handbook/communication/#issues) hygeine for the items in [What's Next and Why](https://about.gitlab.com/direction/analytics/product-analytics/#what-is-next-for-us-and-why) of the direction page as prep for Q1.

## 2023-10-23
1. **N** - Get blog draft into review by blog team
1. **N** - Create a new demo for Field/Customer outreach
1. **Y** - Finalize the [Group OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/4467) for Q4
1. **Y** - Review and Update any direction content ahead of Q4 Kickoff
    - [Customizable Dashboards](https://about.gitlab.com/direction/customizable-dashboards/) direction page was merged and the [PA Group direction](https://about.gitlab.com/direction/analytics/product-analytics/) page was updated.

## 2023-10-16
1. **N** - Get blog draft into review by blog team
1. **N** - Outreach to Beta Prospects
1. **Y** - Refine Q4 OKRs
1. **N** - Move Custom Dashboard Direction into the next phase of review

## 2023-10-09
1. **N** - Get blog draft into review by blog team
1. **Y** - Get the Custom Dashboard direction MR into broader review
1. **Y** - Milestone cadence work (kickoff calls, planning prep, etc.)
1. **Y** - Create a plan for Sales Enablement materials for PA

## 2023-10-02
1. **Y** - Write draft for "Building GitLab with GitLab: Product Analytics"
1. **N** - Finish Tablaeu setup / confirm Analytics dashboard migration

## 2023-09-25
1. **Y** - Create materials for next pricing checkin
1. **N** - Write draft for "Building GitLab with GitLab: Product Analytics"
1. **N** - Finish Tablaeu setup / confirm Analytics dashboard migration

## 2023-09-18
1. **N** - Move pricing proposal forward with more updates and content prep based on new launch checklist
    - Updated deck, more content coming
1. **Y** - Write a blog post proposal for Product Analytics direction and status update
1. **Y** - Any help for onboarding first external users to Product Analytics

## 2023-09-11
1. **Y** - Update pricing recomendation
1. **Y** - Assign coverage issue todos
1. **Y** - Outreach to field team about upcoming beta for Product Analytics
    1. Done week of the 18th

## 2023-09-04 
1. **N** - Willingness To Pay Survey analysis
    - Need a few more responses to get to our goal.
1. **Y** - Move new SKU issue forward to prepare for Q4 prioritization
1. **Y** - Move feedback mechanisms OKR forward
    - Link to the feedback issue added to docs page
    - Future implementation issues created

## 2023-08-28
1. **Y** - Move GA Fulfillment work forward by scoping MVC options identifed by group
1. **Y** - Finish 360 reviews
1. **Y** - Provide updates to potential first customers
1. **N** - Move feedback mechanisms OKR forward

## 2023-08-21
1. **Y** - Move GA Fulfillment work forward by getting DRIs for each functional group
    - Met with key stakeholders and have DRIs lined up for each function.
1. **Y** - Present at Direction Deep dive
1. **N** - Provide updates to first external users for PA
1. **Y** - Gather ideas from team for more user feedback during Beta

## 2023-08-14
1. **Y** - Release the WTP survey
    - Released to UX Recruiting for distribution the week of 2023-08-21
1. **Y** - Move GA Fulfillment work forward by scoping the MVC options
    - Meeting booked with internal stakeholders to get DRIs for each for scoping
1. **N** - Create a copy of the Anlaytics Stage deck for the upcoming deep dive
    - Copy made, updates not applied

## 2023-08-07
1. **Y** - Move the WTP forward by putting the survey into Qualtrics
    - Received feedback from various parties, incorporating next week.
1. **Y** - Move Usage Quota/Fulfillment work forward by incorporating feedback from UQ wireframe, validating data flow for billing with counterparts in Fulfillment
    - Usage quota design is ready to goals
    - Setup meeting with Biz Tech to talk scheduling for next week
1. **Y** - Update Personal Growth goals for Q3

## 2023-07-31
1. **Y** - Move the WTP study forward by drafting the survey for review with pricing team
1. **Y** - Move Usage Quota work forward by creating issues and wireframe for the page
1. **Y** - Move Billing work forward by identifying more concrete work streams

## 2023-07-24
1. **Y** - Validate the functionality to create a dashboard with data from another project
    - Validated no that does not work as expected. Follow-up issues created.
1. **Y** - Refine Product Analytics Usage Quota USM to get to an MVC with Kevin
    - MVC features set and found research from some old projects to review and iterate with.
1. **Y** - Move the WTP study forward
    - Started feature list for survey and screener criteria for review this week with pricing team.
1. **Y** - Freshen up Field Team Tracking epic / issues

## 2023-07-17
1. **Y** - Incorporate any feedback from pricing proposal review w/ Justin
1. **Y** - Finish up pending MRs for category names/direction
1. **Y** - Review/Update release epics to clarify the work needed to be done towards the objectives
1. **N** - Review/Update the customer tracking epic and revive any stale conversations about Beta participation

## 2023-07-10
1. **Y** - Deliver pricing deck to Justin before 2023-07-12 for review
1. **Y** - Slice USM for usage into releases w/ learning goals to move to design/solution validation
1. **Y** - Tag latest usability videos for RITE
1. **N** - Finish up pending MRs for category names/direction
    - Direction update merged, category naming still pending

## 2023-07-03
1. **Y** - Get pricing proposal ready for review with section director
1. **N** - Resolve MR on new category name
1. **Y** - Hold and synthesize data from customer feedback sessions from internal customers
1. **Y** - Iterate on JTBD w/ UX for Dashboard Creation, Visualization Designer, Usage monitoring
1. **Y** - Move [Usage USM](https://gitlab.com/gitlab-org/analytics-section/product-analytics/general-discussion/-/issues/29) forward by drafting and grouping tasks

## 2023-06-25
1. **Y** - Move pricing proposal forward to review with Section leader
    - Models updated after review with Sam and review shceduled.
1. **N** - Resolve MR for category name change(s)
    - Reviewers unavailable between PTO/Holiday
1. **Y** - Move validation of custom dashboard creation forward, scope down first deliverable.
1. **Y** - Kickoff User Story Mapping for Usage Qutoa/Billing flows

## 2023-06-19 (short week w/ holiday and F&F day)
1. **Y** - Move pricing proposal forward to review with Stage manager
1. **N** - Move validation of custom dashboard creation forward, scope down first deliverable.
    - We had good feedback from users in the unmoderated reviews. Scoped down issue not done.
1. **N** - Resolve MR for category name change(s)

## 2023-06-12
1. **Y** - Move pricing proposal forward
    - Models built and assumptions identified.
1. **Y** - Move internal customer Product Analytics onboarding forward
    - Integration with Pajamas project done, working with Handbook team on cookie needs.
1. **Y/N** - Complete custom dashboard creation flow validation with internal customers
    - Moderated sessions completed and some insights gathered. Moving to unmoderated testing next week to complete.

## 2023-06-05
1. **Y** - Record new demo for Product Analytics.
    - [2023-06-09](https://youtu.be/LPKa1-ypYuI) - Default dashboards, preview visualization designer.
1. **Y** - 16.2 milestone planning
    - [Planning issue](https://gitlab.com/gitlab-org/analytics-section/product-analytics/general-discussion/-/issues/24)
1. **Y** - Gather feedback on custom dashboard creation follow
    - 1 interview conducted and 2 scheduled for following week with internal users. External users proving difficult to find.
1. **N** - Iterate on pricing proposal

## 2023-05-29 (Short week and in training 2 days)
1. **Y** - Training Wednesday and Thursday
    - Passed both certifications! ;)
1. **N** - Record new demo for Product Analytics
1. **N** - 16.2 milestone planning

## 2023-05-22
1. **Y** - Move pricing propsoal forward by creating some simple models for discussion
    - Simple models listed and created and started getting feedback.
1. **N** - Update GDK for latest ruby version
1. **N** - Complete a competitive review lite issue
    - Issue started but not complete.

## 2023-05-15
1. **Y** - Update USM/Epics based on team feedback, record a walk through of Experimental and Beta release slices.
2. **N** - Setup GDK again
3. **Y** - Start booking time with SA team to talk Product Analytics
4. **N** - Complete a competitive review lite issue

## 2023-05-08
1. **Y** - Find the next user(s) to onboard to product analytics
    - [Internal issue to track setup](https://gitlab.com/gitlab-org/gitlab/-/issues/410227)
1. **N** - Setup GDK again
    - Talking with field team to work on number 1 took priority
1. **Y** - Update release epics with deeper user stories
    - Updated some stories based on feedback from UX, more to come and direction update
1. **Y** - Normal release cadence work
    - 16.1 kickoff [recorded](https://youtu.be/VC8GoAqPtLY)
    

## 2023-05-01
1. **N** - Setup GDK
    - Partially. I didn't get the EE license to work so retrying this week after deleting and reinstalling the GDK.
1. **Y/N** - Wrapup pending operational issues to onboard first customer to Product Analytics
    - Waiting on an external group but our todos are done here for the most part.
1. **N** - Create welcome/setup video for Product Analtyics Experimental users
    - Deferring this until the conversion to Snowplow from Jitsu is done
1. **Y** - Additional field team outreach for Product Analytics Experimental Phase (8 chats this week)
1. **Y** - Create 1 competitive review lite issue

## 2023-04-24
1. **N** - Fill in Product Analytics Customer onbaording FAQ, create welcome and setup videos.
    - FAQ written and merged, video pending.
1. **N** - Wrapup pending operational issues to onboard first customer to Product Analytics
    - Half done, TBD is infrastructure hosting handoff.
1. **Y** - Additional field team outreach for Product Analytics Experimental Phase
    - Field FYI issue created and additional direct outreach ongoing.
1. **Y** - Fill in slides for Group Conversation

## 2023-04-10
1. **Y** - Finish demo project setup with events showing up in dashboard
1. **Y** - Keep prospecting for Product Analtyics Alpha
1. **N** - Fill in Product Analytics Customer onbaording FAQ, create welcome and setup videos.
    - MR open to create FAQ, script for video written but not recorded.
1. **Y** - Clear the decks for PTO next week
1. **Y** - Product Cadence work - Finalizing 16.0 release, Merge or handoff any pending MRs, etc.

## 2023-04-03
1. **Y** - Turn Sales focused deck into 1 pager for Field teams
1. **N** - Setup a project that uses Product Analytics
    - Project created, missing AppID to send events.
1. **Y** - Fill in / share coverage issue for time off in April
1. **N** - Setup GDK for Product Analytics
1. **N** - Fill in Product Analytics Customer onbaording FAQ, create welcome and setup videos.

## 2023-03-27
1. **Y** - Finish Sales focused deck for Alpha/Beta recruitment
1. **Y** - Add links to Tim's Analytics Vision List doc to existing issues/epics created and competitors who have the functionality
1. **N** - Create onboarding artifacts for first external user / Alpha for Product Analytics
    - Issue templates and list of artifacts needed was created, content tbd.
1. **N** - Stretch - Setup a project that uses Product Analytics

## 2023-03-20
1. **Y** - Review existing Product Analytics Research
1. **N** - Create Sales focused deck for Alpha/Beta recruitment
1. **Y** - Follow-up/handoffs from Opportunity Canvas review
1. **N** - Add links to Tim's Analytics Vision List doc to existing issues/epics created and competitors who have the functionality
1. **N** - Stretch - Setup a project that uses Product Analytics

## 2023-03-13
1. **Y** - Schedule 3 more coffee chats w/ field team to source prospective Alpha customers
1. **N** - Review existing Product Analytics Research
    - 4 of 6 reviewed.
1. **N** - Setup a project that uses Product Analytics
1. **N** - Book a course for 0->1 learning

## 2023-03-06
1. **Y** - Create walkthrough video of Product Analytics features for blog
1. **Y** - Update Goals Issue for PA specific tasks
1. **N** - Deliver CI_JOB_TOKEN settings walkthrough video
    - Delivered a walkthrough of bug(s) I was seeing, resulted in a UI fix and figured out the workflow didn't apply to public projects.

## 2023-02-27
1. **Y** - Fill in 15.11, 16.0 and 16.1 Pipeline Execution Planning issues 
1. **Y** - Review Planning issue for 15.11 Product Analytics release
1. **Y** - Review direction page/decks for Product Analytics

## 2023-02-20
1. **Y** - Updates to direction pages as needed from CI Events research
1. **Y** - Review existing research for CI Analytics/Observability and update Epic(s) and future research as needed for second half of the year 
1. **N** - Fill in 15.11, 16.0 and 16.1 Pipeline Execution Planning issues 
1. **Y** - Finish adding transition tasks to transition issue

## 2023-02-13
1. **Y** - Work with UX to document insights from the [survey done](https://gitlab.com/gitlab-org/ux-research/-/issues/2034#note_1265621626) last week for leveraging CI-based automation for non CI tasks
1. **N** - Updates to direction pages as needed from CI Events research
1. **Y** - Update [Merge Trains Direction](https://about.gitlab.com/direction/verify/merge_trains/) / [Vision Epic](https://gitlab.com/groups/gitlab-org/-/epics/5122) for GH competitive updates
1. **N** - Review existing research for CI Analytics/Observability and update Epic(s) and future research as needed for second half of the year

## 2023-02-06
1. **Y** - Schedule Canvas Review for CI Vsibility Oppt Canvas
1. **N** - Work with UX to document insights from the [survey done](https://gitlab.com/gitlab-org/ux-research/-/issues/2034#note_1265621626) last week for leveraging CI-based automation for non CI tasks.
1. **Y** - Document any follow-ups after Community Office Hours in [Configure CI Minutes Limits per project](https://gitlab.com/groups/gitlab-org/-/epics/6378) epic.

## 2023-01-30
1. **N** - Move Draft of acting GMP blog into review
1. **Y** - Promote office hours for CI Minutes / prep internal issues and epics
    - Booked as a [meetup](https://www.meetup.com/gitlab-virtual-meetups/events/291366736/) for Thursday Feb 9th
1. **Y** - Create CI/CD Feature comparison presentation for upcoming customer call
    - Adding finishing touches to slides created last week
1. **N** - Record overview for CI Visibility Category Oppt canvas and submit for review
1. **Y** - Setup Epic tree(s) for next steps of CI_JOB_TOKEN permissions work
    - [Secure CI_JOB_TOKEN Workflows](https://gitlab.com/groups/gitlab-org/-/epics/6546) updated and child epic(s) created or updated along with update to CI Direction page.

## 2023-01-23
1. **N** - Move Draft of acting GMP blog into review
1. **Y** - Schedule Office Hours for CI Minutes Limits next steps
1. **N** - Fill in Oppty Canvas Lite issue and record overview for CI Visibility Category
1. **N** - Create CI/CD Feature comparison presentation for upcoming customer call
1. **Y** - Create broadcast issue message for CI_JOB_TOKEN change coming in 16.0

## 2023-01-16
1. **N** - Move Draft of acting GMP blog into review
1. **Y** - Record videos of task walkthroughs for Verify Usability Scorecard
1. **N** - Schedule Office Hours for CI Minutes Limits next steps
1. **Y** - Monthly todos for release/PI reviews/Customer escalations

## 2023-01-09 (Short Week F&F day)
1. **Y** - Publish [recap](https://gitlab.com/gitlab-org/ux-research/-/issues/2119#note_1236531569) / [direction](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/117963) updates from Secrets Management Think Big
1. **Y** - Create FY24 overview deck for Pipeline Execution
1. **N** - Move Draft of acting GMP blog into review
